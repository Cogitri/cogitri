# Copyright 2012 NAKAMURA Yoshitaka
# Copyright 2013 Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
require systemd-service

if ever is_scm; then
    SCM_zfsimages_REPOSITORY="https://github.com/zfsonlinux/zfs-images.git"
    SCM_SECONDARY_REPOSITORIES="zfsimages"
    SCM_EXTERNAL_REFS="scripts/zfs-images:zfsimages"
fi

require github [ user=zfsonlinux release=${PNV} suffix=tar.gz ]

export_exlib_phases src_prepare src_install pkg_postinst

HOMEPAGE="http://zfsonlinux.org/"
SUMMARY="Native ZFS for Linux"
DESCRIPTION="
The ZFS on Linux project provides a feature-complete implementation
of the ZFS file system (as defined by the OpenZFS project).

ZFS is a combined file system and logical volume manager.
The features of ZFS include protection against data corruption, support for high
storage capacities, efficient data compression, integration of the concepts of
filesystem and volume management, snapshots and copy-on-write clones, continuous
integrity checking and automatic repair, RAID-Z and native NFSv4 ACLs.
"

LICENCES="CDDL-1.0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        sys-libs/spl[~${PV}]
    suggestion:
        sys-boot/dracut [[ description = [ For generating initramfs which include ZFS' modules ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # Note that this only enables installation of systemd-units
    --enable-systemd

    --with-config=user
    --with-dracutdir=/usr/$(exhost --target)/lib/dracut
    --with-systemdmodulesloaddir=/usr/$(exhost --target)/lib/modules-load.d
    --with-udevdir=/usr/$(exhost --target)/lib/udev
    --with-systemdunitdir="${SYSTEMDSYSTEMUNITDIR}"
    --with-systemdpresetdir="${SYSTEMDSYSTEMUNITDIR}-preset"
    --with-mounthelperdir=/usr/$(exhost --target)/bin
)

# XXX: need root privileges. also, kernel modules should be build to run test
RESTRICT="test"

AT_M4DIR=( config )

zfs_src_prepare() {
    # Use prefixed cpp
    edo sed -i -e "s/cpp/$(exhost --target)-cpp/" config/kernel.m4

    eautoreconf
}

zfs_src_install() {
    default

    edo rmdir "${IMAGE}"/usr/$(exhost --target)/include/libzfs/linux

    dodir /usr/src/${PNV}
    edo mv * "${IMAGE}/usr/src/${PNV}"
    edo find "${IMAGE}" -type d -empty -delete
}

zfs_pkg_postinst() {
    elog "This package only builds ZFS' helper tools"
    elog "The kernel module source has been installed into /usr/src/${PNV}"
    elog "You have to build it yourself now and for every consequent kernel update:"
    elog "# cd /usr/src/${PNV}"
    elog "# ./configure --with-config=kernel --with-linux=<pathtoyourkernelsource>"
    elog "# make && make install"
}

